<?php
echo "<h1> Test de connexion à la BDD</h1>";

$mysqli = new mysqli("db", "demoUser", "demoPass", "demoDb");

if ($result = $mysqli->query("SELECT md5('Patate')")) {
    echo "<p> La connexion à la base de données fonctionne.</p>";
    printf("Select a retourné %d lignes.\n", $result->num_rows);

    /* Libération du jeu de résultats */
    $result->close();
}
else {
    echo "<p> Attention! : la connexion à la base de données ne fonctionne pas.</p>";
}

$mysqli->close();

phpinfo();
?>
